#!/bin/bash
module purge
# load a recent gcc compiler suite
module load openmpi-1.8.4
module load python-2.7.6

#go to home directory
cd $HOME

export HPGMG=hpgmg
mkdir $HPGMG
cd $HPGMG

# get reference benchmark code
# check for more appropriate tuned versions
# at www.https://hpgmg.org/
git clone https://bitbucket.org/hpgmg/hpgmg.git
cd hpgmg
./configure --CC=mpicc
make -j3 -C build

# run HPGMG on 2 nodes (each 2x10 chips) using 2 processes of 10 OpenMP threads.
export OMP_NUM_THREADS=10
srun --ntasks=2   --ntasks-per-node=2  --cpus-per-task=10 --mem=20000  --cpu_bind=verbose,cores -t 00:15:00  mpirun  ./build/bin/hpgmg-fv 7 10