!#/bin/bash
# Go to home directory
cd $HOME
# load compiler environment
module load openmpi-1.8.4

export GRAPH500=graph500
mkdir $GRAPH500
cd $GRAPH500

# get reference benchmark code
#git clone https://github.com/graph500/graph500
wget www.graph500.org/sites/default/files/files/graph500-2.1.4.tar.bz2
tar -xvf graph500-2.1.4.tar.bz2
cd graph500-2.1.4
cd mpi
make
# create submission script
touch subscript
echo '#!/bin/bash ' >> subscript
echo '#SBATCH -N 1 ' >> subscript
echo '#SBATCH --ntasks-per-node=16 ' >> subscript
echo '#SBATCH --mem=20000' >> subscript
echo 'module load openmpi-1.8.4' >> subscript
echo ' ' >>  subscript
echo '# run code' >> subscript
echo 'mpirun graph500_mpi_simple 5' >> subscript
# run code by submitting script
sbatch subscript